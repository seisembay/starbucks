//
//  CardsCollectionViewCell.swift
//  starbucks_behance_rd
//
//  Created by Islam Seisembay on 11/12/2019.
//  Copyright © 2019 seisembay. All rights reserved.
//

import UIKit

class CardsCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var cardImageView: UIImageView!
    
}
