//
//  CustomTabBarViewController.swift
//  starbucks_behance_rd
//
//  Created by Islam Seisembay on 07/12/2019.
//  Copyright © 2019 seisembay. All rights reserved.
//

import UIKit

class CustomTabBarViewController: UITabBarController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tabBar.bounds.origin.y = -150
        tabBar.backgroundColor = UIColor.white
        
        // Do any additional setup after loading the view.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        UIView.animate(withDuration: 1.3, delay: 0.3, options: .curveEaseInOut, animations: {
            self.tabBar.bounds.origin.y = 0
        }, completion: nil)
    }

}
