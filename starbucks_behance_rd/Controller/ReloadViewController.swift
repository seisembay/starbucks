//
//  ReloadViewController.swift
//  starbucks_behance_rd
//
//  Created by Islam Seisembay on 11/12/2019.
//  Copyright © 2019 seisembay. All rights reserved.
//

import UIKit

class ReloadViewController: UIViewController {
    
    @IBOutlet weak var balanceView: UIView!
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var recentViewTableView: UIView!
    
    var cardImages: [UIImage] = [UIImage(named: "1")!, UIImage(named: "2")!, UIImage(named: "3")!]
    
    override func viewWillAppear(_ animated: Bool) {
        UIView.animate(withDuration: 0.6, delay: 0, options: .curveEaseInOut, animations: {
            self.navigationController?.isNavigationBarHidden = false
        }, completion: nil)
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
//        var backButtonBackgroundImage = #imageLiteral(resourceName: "back")
//
//        backButtonBackgroundImage =
//            backButtonBackgroundImage.resizableImage(withCapInsets:
//                UIEdgeInsets(top: 0, left: backButtonBackgroundImage.size.width - 1, bottom: 0, right: 0))
//
//        let barAppearance =
//            UINavigationBar.appearance(whenContainedInInstancesOf: [
//            ])
//        barAppearance.backIndicatorImage = backButtonBackgroundImage
//        barAppearance.backIndicatorTransitionMaskImage = backButtonBackgroundImage
//
//        // Provide an empty backBarButton to hide the 'Back' text present by default in the back button.
//        let backBarButtton = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
//        navigationItem.backBarButtonItem = backBarButtton
        
        let width = view.frame.size.width - 60
        let height: CGFloat = 220
        let layout = collectionView.collectionViewLayout as! UICollectionViewFlowLayout
        layout.itemSize = CGSize(width: width, height: height)
        
        balanceView.layer.borderWidth = 0.6
        balanceView.layer.borderColor = UIColor(white: 0, alpha: 0.2).cgColor
        balanceView.layer.cornerRadius = 10
        
        recentViewTableView.layer.borderWidth = 0.6
        recentViewTableView.layer.borderColor = UIColor(white: 0, alpha: 0.2).cgColor
        recentViewTableView.layer.cornerRadius = 10
        
    }

}

extension ReloadViewController: UICollectionViewDelegate, UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return cardImages.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "Card", for: indexPath) as! CardsCollectionViewCell
        cell.cardImageView.image = cardImages[indexPath.row]
        cell.cardImageView.contentMode = .scaleAspectFill
        cell.layer.cornerRadius = 10

        return cell
    }
    
}
