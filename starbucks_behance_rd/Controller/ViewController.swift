//
//  ViewController.swift
//  starbucks_behance_rd
//
//  Created by Islam Seisembay on 04/12/2019.
//  Copyright © 2019 seisembay. All rights reserved.
//

import UIKit
import Lottie

class ViewController: UIViewController, UIScrollViewDelegate, UIGestureRecognizerDelegate {
    
    @IBOutlet weak var animView: AnimationView!
     
    @IBOutlet weak var balanceOverallView: UIView!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var secondBlockStack: UIStackView!
    
    @IBOutlet weak var logoImageView: UIImageView!
    @IBOutlet weak var cityBgImageView: UIImageView!
    @IBOutlet weak var bgContourImageView: UIImageView!
    @IBOutlet weak var bikeImageView: UIImageView!
    
    @IBOutlet weak var contView: UIView!
    @IBOutlet weak var cardsTalbeContView: UIView!
    
    var contourConstr: NSLayoutConstraint!
    var cityConstr: NSLayoutConstraint!
    var bikeConstr: NSLayoutConstraint!
    
    var balanceOverallViewYAnchorConstr: NSLayoutConstraint!
    var secondBlockConstr: NSLayoutConstraint!
    var thirdBlockConstr: NSLayoutConstraint!
    
    override func loadView() {
        super.loadView()
        self.navigationController?.isNavigationBarHidden = true
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = true
        animView.play()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        viewsFadeIn()
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        scrollView.delegate = self
        scrollView.isDirectionalLockEnabled = true
        scrollView.contentSize.width = view.frame.width
        
        navigationController?.navigationBar.isTranslucent = true
        navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        navigationController?.navigationBar.shadowImage = UIImage()
        
        setConstr()
        setViews()
    }
    
    

    func setViews() {
        
        animView.transform = CGAffineTransform(rotationAngle: CGFloat.pi)
        animView.animation = Animation.named("yellowWaves")
        animView.loopMode = .loop
        animView.contentMode = .scaleAspectFill
        animView.play()
        
        addParallaxToView(vw: cityBgImageView, amount: 40)
        addParallaxToView(vw: bgContourImageView, amount: 60)
        addParallaxToView(vw: bikeImageView, amount: 20)
        addParallaxToView(vw: animView, amount: 20)
        
        balanceOverallView.layer.shadowColor = UIColor.black.cgColor
        balanceOverallView.layer.shadowOpacity = 0.1
        balanceOverallView.layer.shadowOffset = CGSize(width: 0.0, height: 3.0)
        balanceOverallView.layer.shadowRadius = 3
        balanceOverallView.layer.cornerRadius = 10
        balanceOverallView.layer.borderWidth = 1
        balanceOverallView.layer.borderColor = UIColor(red: 233.0/255.0, green: 223.0/255.0, blue: 212.0/255.0, alpha: 1).cgColor
        balanceOverallView.alpha = 0
        
        contView.layer.borderWidth = 0.6
        contView.layer.borderColor = UIColor(white: 0, alpha: 0.2).cgColor
        contView.layer.cornerRadius = 10
        
        bgContourImageView.alpha = 0
        cityBgImageView.alpha = 0
        bikeImageView.alpha = 0
        
        logoImageView.alpha = 0
        
        self.tabBarController?.tabBar.selectionIndicatorImage = .none
        
        self.tabBarController?.tabBar.items![0].image = UIImage(named: "tabbar_home")
        self.tabBarController?.tabBar.items![0].selectedImage = UIImage(named: "tabbar_home_selected")
        
        self.tabBarController?.tabBar.items![1].image = UIImage(named: "tabbar_card")
        self.tabBarController?.tabBar.items![1].selectedImage = UIImage(named: "tabbar_card_selected")
        
        self.tabBarController?.tabBar.items![2].image = UIImage(named: "tabbar_cofefe")
        self.tabBarController?.tabBar.items![2].selectedImage = UIImage(named: "tabbar_cofefe_selected")
        
        self.tabBarController?.tabBar.items![3].image = UIImage(named: "tabbar_reward")
        self.tabBarController?.tabBar.items![3].selectedImage = UIImage(named: "tabbar_reward_selected")
        
        self.tabBarController?.tabBar.items![4].image = UIImage(named: "tabbar_location")
        self.tabBarController?.tabBar.items![4].selectedImage = UIImage(named: "tabbar_location_selected")
        
        self.tabBarController?.tabBar.tintColor = .clear
    
    }
    
    func setConstr() {
        balanceOverallViewYAnchorConstr = balanceOverallView.centerYAnchor.constraint(equalTo: logoImageView.bottomAnchor, constant: 700)
        balanceOverallViewYAnchorConstr.isActive = true
        secondBlockConstr = secondBlockStack.centerYAnchor.constraint(equalTo: balanceOverallView.bottomAnchor, constant: 200)
        secondBlockConstr.isActive = true
        thirdBlockConstr = cardsTalbeContView.centerYAnchor.constraint(equalTo: contView.bottomAnchor, constant: 500)
        thirdBlockConstr.isActive = true
        
        contourConstr = bgContourImageView.centerYAnchor.constraint(equalTo: logoImageView.bottomAnchor, constant: 192)
        contourConstr.isActive = true
        cityConstr = cityBgImageView.centerYAnchor.constraint(equalTo: logoImageView.bottomAnchor, constant: 270)
        cityConstr.isActive = true
        bikeConstr = bikeImageView.centerYAnchor.constraint(equalTo: logoImageView.bottomAnchor, constant: 320)
        bikeConstr.isActive = true
        
    }

    func viewsFadeIn() {
        balanceOverallViewYAnchorConstr.constant = 356
        secondBlockConstr.constant = 42
        thirdBlockConstr.constant = 130
        
        contourConstr.constant = 180
        cityConstr.constant = 204
        bikeConstr.constant = 162
        
        UIView.animate(withDuration: 1.2, delay: 0.3, options: .curveEaseInOut, animations: {
            self.balanceOverallView.alpha = 1
            self.bgContourImageView.alpha = 1
            self.cityBgImageView.alpha = 1
            self.bikeImageView.alpha = 1
            self.logoImageView.alpha = 1
            self.view.layoutIfNeeded()
        }, completion: nil)
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
//        print()
//        print(scrollView.panGestureRecognizer.translation(in: self.view))
//        print(scrollView.bounds.origin.y)
        
        bgContourImageView.frame.origin.y -= scrollView.panGestureRecognizer.translation(in: self.bikeImageView).y / 280
        //cityBgImageView.frame.origin.y += scrollView.panGestureRecognizer.translation(in: self.bikeImageView).y / 280
        bikeImageView.frame.origin.y += scrollView.panGestureRecognizer.translation(in: self.bikeImageView).y / 240
        
        
        if bikeImageView.frame.origin.y > CGFloat(37) || bikeImageView.frame.origin.y < CGFloat(20) {
            UIView.animate(withDuration: 0.5) {
                
                self.bikeImageView.frame.origin.y = CGFloat(37)
                self.bgContourImageView.frame.origin.y = CGFloat(67)
                //self.cityBgImageView.frame.origin.y = CGFloat(87)
            }
        }
        
        //print(cityBgImageView.frame.origin.y)
    }
    
    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        scrollView.panGestureRecognizer.setTranslation(.zero, in: self.view)
    }
    
    func addParallaxToView(vw: UIView, amount: Int) {
        let horizontal = UIInterpolatingMotionEffect(keyPath: "center.x", type: .tiltAlongHorizontalAxis)
        horizontal.minimumRelativeValue = amount
        horizontal.maximumRelativeValue = -amount

        let vertical = UIInterpolatingMotionEffect(keyPath: "center.y", type: .tiltAlongVerticalAxis)
        vertical.minimumRelativeValue = amount
        vertical.maximumRelativeValue = -amount

        let group = UIMotionEffectGroup()
        group.motionEffects = [horizontal, vertical]
        vw.addMotionEffect(group)
    }
    
}

